#Temel Docker Komutları
Başlangıç olarak docker komutları için `docker komut--help` komutu kullanılarak o komut hakkında istenen gerekli tüm bilgilere docker dökümantasyonu aracılı ile ulaşılır.
<br/>
<br/>
`docker search image_name`
<br/>
image dockerhub da aranır ve sonuçlar kullanıcıya döndürülür.
<br/>
<br/>
Bu komutla birlikte kullanılabilir opsiyonlar
--all , -a Bütün containerları listeler
--filter , -f Kondisyona göre çıktıları filtreler
--format Pretty-print formatında containerları listeler
--last , -n -1 “n” tane oluşturulmuş son containerları listeler
--latest , -l Oluşturulmuş son containerları listeler
--no-trunc Çıktıyı olduğu gibi basmaya yarar (bkz. Truncation)
--quiet , -q Sadece IDleri gösterir
--size , -s Toplam dosya boyutlarını gösterir
<br/>
<br/>
`docker images`
<br/>
sistemde indirilmiş olan image'ler listelenir.
<br/>
<br/>
`docker ps`
<br/>
o anki çalışan konteynır'ler listelenir.
-a opsiyonu ile çalışan ve çalışıp durmuş olan tüm konteynırlar listelenir.
<br/>
<br/>
`docker ls`
<br/>
 bulunan konteynırlar listelenir.
 <br/>
 <br/>
`docker pull image_name`
<br/>
image dockerhub'dan indirilir.
<br/>
<br/>
`docker volume create volumeAdi`
<br/>
volume oluşturulur.
 bu komutla birlikte kullanılabilir opsiyonlar
--driver , -d Volume için driver adı tanımlama
--label Volume için metadata tanımlama
--name Volume adı tanımlama
--opt , -o Driver’a özel opsiyon tanımlama
<br/>
<br/>
`docker rename containerAdi yeniContainerAdi`
<br/>
Konteynırın adı değiştirilir.
<br/>
<br/>
`docker update ***--cpu-shares 512 -m 300M* containerAdi`
<br/>
Konteynır üzerinde güncelleştirmeler yapılır.
***Değişken kısım*
<br/>
<br/>
`docker wait nginx`
<br/>
Konteynırı beklemeye alır.
<br/>
<br/>
`docker kill nginx`
<br/>
Konteynırı durdurur.
<br/>
<br/>
`docker rm docker ps -a -q`
`docker rm $(docker ps -a -q)`
<br/>
Durdurulmuş olan tüm konteynır'ları siler.
<br/>
<br/>
`docker rmi $(docker images -a -q)`
<br/>
Varolan tüm image'leri siler.
<br/>
<br/>
`docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)`
<br/>
Çalışan tüm konteynırlar durdurulur ve silinir.
<br/>
<br/>
`docker container commit container_id`
<br/>
Mevcut bir containerdan bir image oluşturulur ve lokalde saklanır.
 <br/>
 <br/>