#Docker Mimarisi

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker sunucu istemci tabanlı çalışan konteynırlaştırma platformudur. Docker bünyesinde bulunan; docker client, docker host ,network,storage component ve docker hub'dan oluşur. Görselde de birbirleri ile etkileşimleri gözlemlenebilir.

![Docker mimarisi](/photo/Docker_Architecture.png)

##Docker client(Docker istemcisi)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker istemcisi kullanıcının docker ile iletişimi sağlar.Docker istemcisi, arka plandaki bilgisayarla aynı ana bilgisayarda bulunabilir veya uzaktaki bir ana bilgisayardaki bir arka planda çalışabilir.Docker istemcisinin ana amacı bir görüntüyü(image) indirmek (docker pull) ve onu docker host'da çalıştırmaktır.

##Docker host

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker host uygulamaların derlenip çalışabilmesi için gerekli tüm işlem materyallerini bünyesinde barındırarak eksiksiz bir ortam sağlar. Docker host ;docker deamon,docker images,docker container,docker network and docker storage'dan oluşur.Docker host kullanıcı arayüzünden, restApi'den gelen ve tüm konteynır tabanlı uygulamalardan gelen istekleri alma ve yönetme sorumluluğuna sahiptir.Ayrıca, hizmetlerini yönetmek için diğer daemonlarla(servlet) iletişim kurabilir.
<br>
---
##Docker Objeleri

Docker uygulaması oluşturmak ve koşturmak için bazı docker objeleri vardır. Bunlar;

###Docker image

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docker image, docker container oluşturabilmek için yalnız salt okunur binary dosyalardır.docker image bir programın bir arayüzün veya bir işletim sisteminin o anki kaydedilmiş görüntüsüdür. yani bir programı veya program parçacığını o anki çalışır haliyle kaydedip saklamak için en kolay yoldur. docker image ismi de buradan gelir. o anı resmetmek anlamı gözetilerek bu isimle isimlendirilmiştir.

###Docker container

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Konteynırlar, uygulamaları çalıştırdığınız kapsüllenmiş ortamlardır. Konteynır, görüntü ve ağ bağlantıları ve depolama seçenekleri dahil, ancak bunlarla sınırlı olmamak üzere, kabın başlatılması sırasında sağlanan diğer yapılandırma seçenekleriyle tanımlanır. Konteynır, görüntü bir Konteynır içine oluşturulurken ek erişim tanımlanmadığı sürece, yalnızca görüntüde tanımlanan kaynaklara erişebilir. Bir Konteynır'ın geçerli durumuna göre yeni bir resim de oluşturabilirsiniz. Konteynırlar VM'lerden çok daha küçük olduğundan, birkaç saniye içinde oluşturulabilir ve sunucu yoğunluğuna karşı çok daha iyi bir performans çıkarırlar.

###Docker network

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker'ın çıkış amaçlarından birisi de sistemlerin birbirine izole olması sistemde çökmenin yaşanması durumunda diğer kısımların,container'ların bundan etkilenmemesi istenir ve beklenir. bunun yanında sistemin kendi içindeki parçalarla loosly coupling yani az bağlantılı az bağımlı olması beklenir. docker default olarak dört çeşit baglantı ile gelir. bunlar;
<br>
Docker Bridge network
<br>
Docker Host Network
<br>
Docker None network
<br>
Docker User-Defined network
<br>


<br>
***Docker Bridge Network*** default olan bağlantıdır ve aksi belirtilmediği takdirde oluşturulan containerlar bu ağa bağlı oluşurlar. bağlantı -docker network disconnect bridge container_name- komutu ile koparılabilir.
<br>
***Docker Host Network*** docker host network bağlantısı ile oluşturulan containerlar hosttaki yani konuktaki tüm baglantı arayüzleri ile etkileşim kurabilir olarak oluşturulacaktır.
<br>
***Docker None network*** bir container’ın herhangi bir network’e sahip olmadığı durumdur. None olarak çalıştırılan container’lar docker network stack’ine alınırlar ancak herhangi bir network configuration’u yapılmaz. Sadece lo interface tanımlı bir şekilde meydana gelirler. O da iç network’de yer alan herhangi bir ip adresine erişmek istenirse dış network’e bulaşmaması için loopback interface’dir.
 <br>
 ***Docker User-Defined network*** kendi tanımladığımız network'ü oluşturmak ve diğer dünyadan sınırlandırmak istediğimizde kullanacağımız bağlantı türüdür.
 <br>
 docker network create --driver bridge NETWORK_ADI
 <br>
 komutu ile oluşturabilir ve içine 
 <br>
docker run -it --network=NETWORK_ADI --name=container_adı a
 <br>
 komutu ile container tanımlayabiliriz.
 <br>
###Docker Storage

 Docker containerlarında veri saklamaya da ihtiyaç duyabiliriz. Verileri konteynırın yazılabilir katmanında saklayabiliriz ama konteynır çalışmadığında veriler kaybolabilir. Docker bunun için bazı çözümler geliştirmiştir.

#### Docker Volume
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Container’lar docker host’u üzerinde stateless olarak yani herhangi bir durum bilgisi tutmadan çalışırlar ve container bir kez silinince içindeki herşey sonsuza dek kaybolur.  Çalıştırdığımız uygulamaların çoğunlukla farklı container’larda sıfırdan ayağa kaldırılarak çalıştırılması, bağımlılıklarının güncellenmiş image’lar üzerinde yönetilmesi gerekliliğini iyi bir proje yönetimi için kaçınılmazdır.Düzenli olarak docker’da çalışan -durum bilgisi ve süregelen veriler içeren- uygulamaların ise tamamen silinip yok olma lüksü yoktur. Üzerinde çalıştığı container silinse de container bağımsız bir şekilde herhangi bir veri kaybetmeden yeni bir container’da çalışır durumda olması beklenir.Uygulamanın konfigürasyon dosyaları, ürettiği log dosyaları, içeride tuttuğu veri deposu, sürekliliği olan veriler ya da başka gereksinimler nedeniyle container bağımsız yönetilmesi gereken verileriniz olabilir. İşte bu durumun doğru yönetilebilmesi için docker volume geliştirilmiş ve kullanıma sunulmuştur.
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Her container çalıştırıldığı sırada , biz tanımlamasak bile kendine bir alan oluşturuyor ve işlem yaptığımız verileri bu oluşturduğu alan üzerinde tutuyor. Bu oluşturulan alana “docker volume” deniliyor. Docker containerın verileri için host üzerinde bir alan belirleyip bu alanı işaretliyor.
<br>
!!!!!!!!!!!!!!!!!!!!!
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Temel olarak docker’ın çalıştığı makine (host) üzerindeki bir alanı çalıştırdığımız container üzerindeki bir alan ile eşliyorsunuz. Dolayısıyla container yok olduğunda da ana makine üzerinde eşlenen volume’ler kayıpsız bir şekilde var olmaya devam ediyor. İsterseniz yeni bir container ayağa kaldırdığınızda bu volume’ler üzerinden çalıştırmaya devam edebiliyorsunuz. Böylece herhangi bir veri kaybı yaşamadan farklı ve temiz container’lar çalıştırabiliyorsunuz.
<br>
!!!!!!!!!!!!!!!!!!!!!
<br>

