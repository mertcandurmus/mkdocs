#Kubernates nedir?
Kubernetes Google tarafından GO dilinde geliştirilmiş Cloud Native Computing Foundation tarafından desteklenen mevcut konteyner haline getirilmiş uygulamalarınızı otomatik deploy etmek, sayılarını arttırıp azaltmak gibi işlemler ile birlikte yönetmenizi sağlayan bir Konteyner kümeleme (container cluster) aracıdır.
<br>
Kubernetes’in yapısına göz atacak olursak master ve minion dedikleri node’lardan oluşuyor. Bu minion dediğimiz node’lar içerisinde pod’lar ve pod’ların içerisinde konteyner’larımız bulunmaktadır.Bu yapı kendi içinde overlay dediğimiz bir network ile haberleşiyor.
<br>
![Kubernates mimarisi](/photo/kubernates.png)
<br>
Master node içerisinde 4 tane temel yapıtaşı vardır.
<br>
Bunlar;
<br>
API Server
<br>
Controller Manager
<br>
Scheduler
<br>
Etcd
<br>
***API Server***:Master sunucumuza gelen tüm REST requestlerin yönetilmesinden sorumludur.Aslınsa Cluster’ın beyni diyebiliriz.Json file’ları ile yönetilir.
<br>
***Controller Manager***:Temel olarak, bir denetleyicidir, kümenin durumunu API Server izleme özelliğiyle izler ve bildirildiğinde, geçerli durumu istenen duruma doğru hareket ettirmek için gerekli değişiklikleri yapar.
<br>
***Scheduler***: Bir pod’un hangi node üzerinde çalışacağına karar verir , kubelet’i tetikler ve ilgili pod ve içindeki konteyner oluşturulur.Kısacası yeni bir pod oluşturulması isteğine karşı API server’ı izler.
<br>
***Etcd***: Coreos tarafında yaratılmış open source distributed,tutarlı ve izlenebilir bir key value store (nosql database) diyebiliriz.Redhat Coreos ‘si satın aldı aslında konteyner teknolojisinde çok güzel hamleler atıyor.
<br>
##Kubernates nasıl çalışır
![Kubernates nasıl çalışır](/photo/kup2.png)
<br>
Kubectl(kubernetes client) isteği API server ‘a iletir.
<br>
API Server isteği kontrol eder etcd ‘ye yazar.
<br>
etcd yazdığına dair bilgilendirmeyi API Server’a iletir.
<br>
API Server ,yeni pod yaratılacağına dair isteği Scheduler ‘a iletir.
<br>
Scheduler, pod ‘un hangi server’da çalışacağına karar verir ve bunun bilgisini API Server’a iletir.
<br>
API Server bunu etcd ye yazar
<br>
etcd yazdığına dair bilgiyi API Server’a iletir.
<br>
API Server ilgili node’daki kubelet’i bilgilendirir
<br>
Kubelet,Docker servisi ile ilgili docker soketi üzerindeki API’yi kullanarak konuşur ve konteyner’ı yaratır.
<br>
Kubelet ,pod’un yaratıldığını ve pod durumunu API Server’a iletir.
<br>
API Server pod’un yeni durumunu etcd’ye yazar.
<br>
###Kubernates ve docker arasındaki benzerlikler 
Benzerlikler

###Kubernates ve docker arasındaki farklılıklar