# Docker Nedir?

Docker, konteynır teknolojisini kullanarak uygulama geliştirmeyi, konuşlandırmayı ve çalıştırmayı kolaylaştıran açık kaynak kodlu bir platformdur.Sanal makinalara kıyasla daha esnek bir yapıya sahiptir.

## Konteynır

Konteynır'ı kısaca açıklayacak olursak, Docker Engine tarafından çalıştırılan ve birbirlerinden izole edilmiş işlemlerin her birine verilen isimdir. Geliştiriciler, geliştirme ortamlarını bu konteynırlar içerisine gerekli konfigürasyonları ile birlikte paketleyerek istedikleri ortama aktarabilirler. Deniz taşımacılığından esinlenilen bu yapı hem geliştiriciler açısından hem de sistem yöneticileri açısından bir çok sorunu ortadan kaldırmaktadır.

## Image (Görüntü)

Docker image adından da anlaşılacağı gibi çalışacak uygulamanızın ve uygulamanızın altyapısında çalışan gerekli işletim sistemi kütüphanelerinin bulunduğu bir yapıdır. İmajları, container yaratmak için gereken talimatların bulunduğu bir şablon olarak düşünebiliriz. docker image build komutu ile bir Dockerfile üzerinden oluşturduğumuz yapılardır.


## Dockerfile

Aşağıdaki resim üzerinden takip edebileceğiniz şekilde aslında Dockerfile image ile ilgili tüm işlemlerin adımlarını barındıran komutlar zincirinden oluşmaktadır. Genel bilgi olarak FROM komutu ile bir base image dosyasını referans olarak belirliyoruz. ENV komutu ile genel değişkenlerin atamasını yapıyoruz, RUN komutu ile container ile birlikte build zamanında yapılması gereken tüm işlemleri tanımlıyoruz. EXPOSE ile sanal docker networkünde kullanılacak portları tanımlayıp en son olarak CMD komutunda container çalıştırıldığında işleme alınacak komutları tanımlıyoruz.


![Dockerfile](/photo/photo1.png)


## Docker ile örnek bir mimari

Aşağıdaki fotoğraftaki örnekten gidecek olursak, sunucumuzda 4 adet docker container çalışıyor. 2 adet Apache web sunucusu, 1 adet mysql veritabanı, 1 adet ssh-bis. Apache sunucular 80 portundan yayın yapacaklar. Bu 80 portundaki yayın container'dan dışarıya (ana (host) sisteme) çıkarken 25890 ve 27568 olarak çıkacaklar. Containerın çalıştığı sistemde web tarayıcı ile http://localhost:25890 adresine girdiğimiz zaman birinci Apache'nin cevabını alırız.

Nginx gibi bir reverse proxy ise gelen istek sub1.domain.com:80'e ise ilk containera (25890), sub2.domain.com:80 ise ikinci containera (27568) yönlendirecek. Bu sayede çok sayıda yazılım bir sistemde birbirinden izole bir halde kolay kurulumla çalışabilecek.

![Docker-örnek- mimari](photo/photo2.png)