#Nasıl Dockerfile oluşturabiliriz?
FROM ubuntu:18.04
<br>
COPY . /app
<br>
RUN make /app
<br>
CMD python /app/app.py
<br>

örneği üzerinden ele almak gerekirse;
<br>***FROM*** 
<br>docker image'nin hangi image'den türetilecegini gösterir. bu sayade de tüm docker imageler üst imageler aracılığıyla türetildiğinden tüm docker imageler arasında bir bağ kurulmuş olur.
<br>***COPY***<br> proje verilerinin projenin hangi dizininden oluşturulacak olan docker image'in hangi dizinine kopyalanacağını docker'a belirten komuttur.
<br>***RUN***<br>uygulamayı kuran komuttur.
<br>***CMD*** <br>uygulama başladığına çalıştırmak istediğimiz komut satırı komutlarını docker'a verdiğimiz komuttur.
<br>
Bunların haricinde diğer dockerfile komutları;
<br>***EXPOSE***<br>
Konteynırın hangi portları dinleyeceği bu komut ile docker'a bildirilir.
<br>***ENV***<br>
Path değişkenini güncellemek ve yönetmek için kullanılır.
<br>***ADD***<br>
<br>***ENTRYPOINT***<br>