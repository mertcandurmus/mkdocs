#Docker Swarm nedir?
Gerçek hayatta birden çok sunucu üzerinde çok sayıda container’ın host edildiği ve çok daha karmaşık kullanımların olduğu durumlar olacaktır. Ve bu sistemlerin ölçeklenebilir yapılar olması gerekliliği ortaya çıkacaktır. Docker container’ların bir orkestratör yardımı ile cluster içerisindeki node’larda yönetilmesi ve bu işlemin otomatize edilmesi büyük çaplı sistemlerin yönetilebilmesini kolaylaştırmakta high availability gibi kavramların uygulanabilmesine imkan tanımaktadır.Docker'ın bu duruma ürettiği çözüm swarm mode olarak adlandırılmaktadır.
<br>
Swarm mode temel olarak sürekli erişilebilirlik(high availability) ve ölçeklenebilirlik(scalability) sorunlarına getirdiği kolay kullanımlı ve hızlı çözüm nedeniyle tercih edilmektedir. Kubernetes(k8s) de aynı işlemleri ve daha çok seçenekle yapabilmektedir. Fakat öğrenim süresi ve kolay kullanım açısından docker swarm bir adım öne çıkmaktadır. swarm yapısal olarak iki tip node’dan oluşur, bunlar : 
<br>
**Manager node**
<br>
**Worker node**
<br>
![Swarm Node](/photo/swarm.jpg)
<br>
###**Manager Node**
<br>
 Swarm cluster üzerindeki işlerin yönetilmesini gerçekleştiren node grubudur. Servislerin yönetilmesi, ölçekleme, sürekli monitör ederek cluster ortamını(cluster state) istenen seviyede tutma(desired state), servisler arası yük dağılımı(load balancing) gibi görevleri vardır.
<br>
###**Worker Node**
<br>
İşlerin yürüdüğü yani container’larımızın çalıştığı node’lara verilen isimdir. Bir swarm cluster’da hiç worker node yokken de cluster tüm işlevini yerine getirebilir fakat sadece worker node olan bir cluster olamaz. Bir worker node sonradan manager yapılabilir(promote).
<br>
##**Docker Swarm nasıl oluşturulur?**

 İlk adımda init yaparak yeni bir swarm cluster oluşturacağız. Swarm cluster yönetimi ile ilgili komutları çalıştırken docker swarm kullanılır. Örnek amaçlı olduğundan 3 manager ve 2 worker için işlemleri tekrarlama gereği olmadığından iki node içeren bir yapı için swarm cluster oluşturmak için init yapalım.
<br>
 **docker swarm init --advertise-addr $(hostname -i)**
 <br>
  komutu ile initialize yapılır.
<br>
Ekran Çıktısı:
<br>
  
Swarm initialized: current node (g1wkk2jnoygiahycglr6nlsle) is now a manager.
<br>
To add a worker to this swarm, run the following command:
<br>
    docker swarm join --token SWMTKN-1-0habm68o4lz3g61dxhsfecq6e2nod75o5cz3bej7r4ac8uprjv-0ds7fwaaud8fylqr2z4uidzz2 
    <br>192.168.0.33:2377
    <br>
To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
<br>
<br>

Cluster hazır ve ilk node olan manager node oluştu, diğer node’umuzu da worker node olarak ekleyelim. Bunun için init yaptıktan sonra dönen açıklama içerisindeki “docker swarm join — token …” şeklinde devam eden komutu kopyalayıp kullanabiliriz veya manager’daki komut satırından “docker swarm join-token worker” ile join token bilgisini alabiliriz. Şimdi ikinci node’umuzu worker olarak ekleyelim. Bunun için ikinci node komut satırından
<br>
 **docker swarm join --token SWMTKN-1-0habm68o4lz3g61dxhsfecq6e2nod75o5cz3bej7r4ac8uprjv-0ds7fwaaud8fylqr2z4uidzz2 192.168.0.33:2377**
 <br>
  komutu ile cluster'a worker node eklenir.
<br>
Ekran Çıktısı:
<br>
“This node joined a swarm as a worker.”
şeklinde ise işlem başarılı bir şekilde gerçekleştirildi demektir.