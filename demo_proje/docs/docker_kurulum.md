#Docker Kurulumu

Docker sisteminin kurulumu sonra derece kolaydır. Linux dünyası için konuşacak olursak pek çok Linux dağıtımında software repository altyapılarında docker uygulaması mevcuttur. Windows ve Mac OS X işletim sistemleri içinde aşağıdaki adımları izleyerek kurabilirsiniz
Uygulamanın kurulması için sadece kullandığımız Linux dağıtımı üzerinde terminal uygulamasını açarız ve aşağıdaki kodu çalıştırırız

“sudo wget -qO- https://get.docker.com/ | sh” (Tırnak işaretleri olmadan)

Kurulum yapıldıktan sonra OS yeniden başlatırsanız Docker uygulaması çalışmaya başlayacaktır. Yada “sudo service docker start” komutu ile servisi hemen çalıştırabilirsiniz.

Docker versiyon bilgilerini “sudo docker version” ile öğrenebilirsiniz.

Sırayla “sudo apt-get update” ve “sudo apt-get upgrade” ile docker versiyonun yeni versiyonu var ise kurulumunu yapabilirsiniz.

windows ve mac ortamları için ise;


[windows için docker](https://www.docker.com/products/docker-desktop)

[mac için docker](https://www.docker.com/products/docker-desktop/)

linkler aracılığı ile çalıştırılabilir dosya indirilir. indirme yapmadan önce üye olunması gerekmektedir. üyelik işlemleri ücretsiz olarak gerçekleştirildikten sonra çalıştıralabilir dosya indirilir ve aşamalar ilerletilmek suretiyle kurulum tamamlanır. 
kurulumun doğru bir şekilde gerçekleştiğinden emin olmak için komut satırına
--
`docker version` 
--
yazılarak kontrol edilebilir.

