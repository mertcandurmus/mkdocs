#Docker Compose Nedir?
Docker Compose, kompleks uygulamaların tanımlanmasını ve çalıştırılmasını sağlayan bir Docker aracıdır. Docker Compose ile birlikte birden fazla container tanımını tek bir dosyada yapabilir, tek bir komut ile uygulamanızın ihtiyaç duyduğu tüm gereksinimleri ayağa kaldırarak uygulamayı çalıştırabilirsiniz.
Docker Compose ile birden fazla container çalıştırabilir, bu containerlardan bazılarının birbirine bağımlı kalmasını isteyebiliriz.
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker compose kullanımı için docker-compose.yml dosyası zorunludur ve adlandırması böyle olmalıdır.
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker-compose.yml dosyasında detayları verilen ve birbirleri ile ilişkileri tanımlanan servisler (Container’lar) tek bir komut ile ayağa kaldırılıp tek bir komut ile durdurulur ve yine tek bir komut ile kaldırılabilirler (silinebilirler).

##Docker Compose ile kullanılabilir docker komutları
<br>
***docker-compose build***
<br>
Compose dosyasında tanımladığınız Container’ları (servisleri) teker teker veya toplu halde build edebilirsiniz.
<br>
***docker-compose up***
<br>
Compose dosyasında tanımlı bütün servisleri ayağa kaldırmak için kullanılır.
<br>
***docker-compose down***
<br>
 Compose tarafından yaratılmış Container’ları (servisleri) öncelikle durdurarak sonra da sistemden kaldırmaktadır.
<br>
***docker-compose run***
<br>
Compose dosyasında tanımlanan bir servis için Compose dosyasında tanımlanmayan bir komut koşturmaktır.
<br>
***docker-compose stop***
<br>
Başlatılan servisleri durdurmak için kullanılır.
<br>
***docker-compose exec***
<br>
Çalışmakta olan Container’da bir komut koşturmak için kullanılır.
<br>
***docker-compose ps***
<br>
Dockor-compose.yml dosyasının olduğu dizinden komut çalıştırılırsa sadece compose ait konteynırlar listelenir.

##Docker Compose kullanım örneği

istenilen bir dizinde meanStackDocker isimli projeyi içinde barındıracağımız bir klasör oluşturulur.
<br>
**cd meanStackDocker**
<br>
###**Angular Kısmı**
klasörün içine girilir ve bir angular projesi oluşturulur.
<br>
**ng new frontEnd**
<br>
ng serve ile proje ayağa kaldırılıp knotrol edilebilir.
<br>
angular uygulaması ile aynı dizine gidip Dockerfile isminde uygulamanın image haline gelmesi için Docker'a vereceğimiz file tanımlanır.
<br>
**cd frontEnd**
<br>
**mkdir Dockerfile** 
<br>
dosya uzantısı yoktur ve dosyanın adının Dockerfile olması önemlidir. aksi takdirde dosyanın image oluşturmak için docker'a özellikle verilmesi gerekir.
#### Angular için Dockerfile oluşturma
<br>
yorum:  Dockerhub üzerinden resmi Node 8 image'ını temel alarak bir image oluştur
<br>
***FROM node:8***
<br>
yorum: Uygulamamızın bulunacağı klasörü oluştur
<br>
***RUN mkdir -p /var/www/app***
<br>
yorum: Komutlarımızın çalıştırılacağı dizini seç
<br>
***WORKDIR /var/www/app***
<br>
yorum: package.json dosyamızı çalışma dizinine kopyala
<br>
***COPY package.json /var/www/app***
<br>
yorum: Bağımlılıkları kur
<br>
***RUN npm install***
<br>
yorum: Tüm proje dosyalarını docker image'ına kopyala
<br>
***COPY . /var/www/app***
<br>
yorum:  Uygulamanın çalışacağı port
<br>
***EXPOSE 4200***
<br>
yorum:  Projeyi ayağa kaldıracak komutu çalıştır
<br>
***CMD ["npm", "start"]***
<br>

aynı dizinde birde .dockerignore dosyası oluşturulmalı
<br>
***node_modules/*** .dockerignore dosyasının içine yazılır
<br>
Son olarak da package.json dosyasındaki start script'i 
<br>
***"start": "ng serve -H 0.0.0.0"***
<br>
şeklinde değiştirilir.

<br>
Docker image oluşturma
<br>
***docker build -t image_adı:image_versiyonu .***
<br>
Oluşturulan image'den cantainer ayaklandırma
<br>
***docker run -d --name container_adı -p 4200:4200 image_adı:image_versiyonu***
<br>
http://localhost:4200  adresine gidildiğinde şöyle bir ekranla karşlaşılır.

![AngularApp](/photo/angular-client.png)

###**ExpressServer Nodejs Kısmı**
Nodejs uygulaması için projeyi oluştrduğumuz dizinde bir klasör oluşturulur.
klasörün içine gidilir.
![NOdejs](/photo/nodejs.png)
resimde görüldüğü gibi dizinler oluşturulur ve 
<br>
***npm install -g express-generator***
<br>
komutu ile express server oluşturulur.
<br>
***express .***
<br>
***npm install***
<br>
komutları ile express server tamamlanır.

#### Express Server için Dockerfile oluşturma

yorum: Dockerhub üzerinden resmi Node 8 image'ını temel alarak bir image oluştur
<br>
***FROM node:8***
<br>
yorum: Uygulamamızın bulunacağı klasörü oluştur
<br>
***RUN mkdir -p /var/www/app***
<br>
yorum:  Komutlarımızın çalıştırılacağı dizini seç
<br>
***WORKDIR /var/www/app***
<br>
yorum: package.json dosyamızı çalışma dizinine kopyala
<br>
***COPY package.json /var/www/app***
<br>
yorum: Bağımlılıkları kur
<br>
***RUN npm install***
<br>
yorum: nodemon'u kur
<br>
***RUN npm install -g nodemon***
<br>
yorum: Tüm proje dosyalarını docker image'ına kopyala
<br>
***COPY . /var/www/app***
<br>
yorum: Uygulamanın çalışacağı port
<br>
***EXPOSE 3000***
<br>
yorum: Projeyi ayağa kaldıracak komutu çalıştır
<br>
***CMD ["npm", "start"]*** 
<br>
<br>
Docker image oluşturma
<br>
***docker build -t image_adı:image_versiyonu .***
<br>
Oluşturulan image'den cantainer ayaklandırma
<br>
***docker run -d --name container_adı -p 3000:3000 image_adı:image_versiyonu***
<br>